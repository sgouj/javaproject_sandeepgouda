package com.allstate.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PaymentsTest {
    private Payment payment;

    SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy HH:mm:ssZ");
    Date now = new Date();

    @BeforeEach
    void setUp() {
        payment = new Payment(1, formatter.format(now), "CREDIT CARD", 12345);
    }

    @Test
    public void getId() {
        assertEquals(1, payment.getId());
    }

    @Test
    public void getPaymentDate() {
        // assertEquals(new Date().getTime(), payment.getPaymentDate().getTime());
    }

    @Test
    public void getCustId() {
        assertEquals(12345,payment.getCustid());
    }

    @Test
    public void getType() {
        assertEquals("CREDIT CARD", payment.getType());
    }


}
