package com.allstate.service;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import com.allstate.exception.DAOException;
import com.allstate.exception.ServiceException;
import com.allstate.services.PaymentService;
import com.mongodb.BasicDBObject;
import org.junit.After;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentServiceTest {

    @Autowired
    PaymentService paymentService;

    @Autowired
    PaymentDao paymentDao;
    
    @Autowired
    MongoTemplate mongoTemplate;

    SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy HH:mm:ssZ");
    Date now = new Date();
    Payment payment = new Payment(19, formatter.format(now), "CREDIT CARD", 111);


    @Test
    public void canSaveSuccess() throws ServiceException, DAOException {
        long save = paymentService.save(payment);
        assertEquals(1,save);
        assertNotNull(paymentService.findById(payment.getId()));
    }

    @Test
    public void findById() throws ServiceException {
        Payment paymentObj = paymentService.findById(payment.getId());
        assertNotNull(paymentObj);
    }

    @Test
    public void findByType() throws ServiceException {
        List<Payment> list;
        list = paymentService.findByType(payment.getType());
        assertNotNull(list.get(0).getType(), "CREDIT CARD");
    }

    @After
    public void Cleanup() throws DAOException {
        Payment payment = paymentDao.findById(19);
        mongoTemplate.remove(payment);
    }


   /* @After
    public void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("payment")) {
               // mongoTemplate.getCollection(collectionName).get
               // mongoTemplate.getCollection(collectionName).
            }
        }
    }*/

}
