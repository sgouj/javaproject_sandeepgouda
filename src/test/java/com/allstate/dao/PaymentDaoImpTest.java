package com.allstate.dao;


import com.allstate.entities.Payment;
import com.allstate.exception.DAOException;
import org.junit.After;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class PaymentDaoImpTest {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    PaymentDao paymentDao;
    SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy HH:mm:ssZ");
    Date now = new Date();

    Payment payment = new Payment(10, formatter.format(now), "CREDIT CARD", 123456);

    @Test
    public void canSaveSuccessfully() throws DAOException {
        mongoTemplate.save(payment);
        assertEquals(payment.getId(), paymentDao.findById(10).getId());
    }

    @Test
    public void rowCountTest() throws DAOException {
        assertTrue(paymentDao.rowCount() > 0);
    }

    @Test
    public void findByTypeTest() throws DAOException {
        mongoTemplate.save(payment);
        assertEquals(payment.getType(), paymentDao.findById(10).getType());
    }

    @After
    public void Cleanup() throws DAOException {
        Payment payment = paymentDao.findById(10);
        mongoTemplate.remove(payment);
    }
}
