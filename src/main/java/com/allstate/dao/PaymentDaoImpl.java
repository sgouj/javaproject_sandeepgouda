package com.allstate.dao;

import com.allstate.entities.Payment;
import com.allstate.exception.DAOException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class PaymentDaoImpl implements PaymentDao {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public long rowCount() throws DAOException {
        long count;
        try{
            Query query = new Query();
            count = mongoTemplate.count(query, Payment.class);
        } catch(Exception exception) {
            throw new DAOException(ExceptionUtils.getMessage(exception) +
                    System.lineSeparator() + ExceptionUtils.getMessage(ExceptionUtils.getRootCause(exception)), exception
            );
        }
        return count;
    }

    @Override
    public Payment findById(int id) {
            Query query = new Query();
            query.addCriteria(Criteria.where("id").is(id));
            Payment payment = mongoTemplate.findOne(query, Payment.class);
//        Payment payment;
//        try {
//            Query query = new Query();
//            query.addCriteria(Criteria.where("id").is(id));
//            payment = mongoTemplate.findOne(query, Payment.class);
//        } catch(Exception exception) {
//            throw new DAOException(ExceptionUtils.getMessage(exception) +
//                    System.lineSeparator() + ExceptionUtils.getMessage(ExceptionUtils.getRootCause(exception)), exception
//            );
//        }

        return payment;
    }

    @Override
    public List<Payment> findByType(String type) throws DAOException {
        List<Payment> payments = null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("type").is(type));
            payments = mongoTemplate.find(query, Payment.class);
        } catch(Exception exception) {
            throw new DAOException(ExceptionUtils.getMessage(exception) +
                    System.lineSeparator() + ExceptionUtils.getMessage(ExceptionUtils.getRootCause(exception)), exception
            );
        }
        return payments;
    }

    @Override
    public int save(Payment payment) throws DAOException {
        Payment p = null;
        try {
            p = mongoTemplate.save(payment);
        } catch(Exception exception) {
            throw new DAOException(ExceptionUtils.getMessage(exception) +
                    System.lineSeparator() + ExceptionUtils.getMessage(ExceptionUtils.getRootCause(exception)), exception
            );
        }
        return p != null ? 1 : 0;
    }
}
