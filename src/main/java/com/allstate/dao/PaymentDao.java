package com.allstate.dao;

import com.allstate.entities.Payment;
import com.allstate.exception.DAOException;

import java.util.List;

public interface PaymentDao {
    public long rowCount() throws DAOException;
    public Payment findById(int id);
    public List<Payment> findByType(String type) throws DAOException;
    public int save(Payment payment) throws DAOException;
}
