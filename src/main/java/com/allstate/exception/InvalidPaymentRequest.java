package com.allstate.exception;

public class InvalidPaymentRequest extends RuntimeException{
    public InvalidPaymentRequest(String message) {
        super(message);
    }
}
