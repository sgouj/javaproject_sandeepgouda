package com.allstate.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class PaymentControllerExceptionHandler {

    @ExceptionHandler(PaymentDetailsNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handlePaymentNotFound(PaymentDetailsNotFoundException ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),request.getDescription(false), HttpStatus.NOT_FOUND.value());
        return new ResponseEntity(errorDetails,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidPaymentRequest.class)
    public final ResponseEntity<ErrorDetails> handleInvalidPayment(InvalidPaymentRequest ex, WebRequest request) {
       ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),request.getDescription(false), HttpStatus.NOT_FOUND.value());
       return new ResponseEntity(errorDetails,HttpStatus.BAD_REQUEST);
    }
}
