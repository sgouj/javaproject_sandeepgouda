package com.allstate.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Document
public class Payment implements Serializable {
    @Id()
    private int id;
    private Date paymentDate;
    private String type;
    private int custid;

    public Payment(int id, Date paymentDate, String type, int custid) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.custid = custid;
    }

    public Payment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentDate=" + paymentDate +
                ", type='" + type + '\'' +
                ", custid=" + custid +
                '}';
    }
}
