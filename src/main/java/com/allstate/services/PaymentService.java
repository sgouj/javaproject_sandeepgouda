package com.allstate.services;

import com.allstate.entities.Payment;
import com.allstate.exception.ServiceException;

import java.util.List;

public interface PaymentService {
    public long rowCount() throws ServiceException;
    public Payment findById(int id);
    public List<Payment> findByType(String type) throws ServiceException;
    public int save(Payment payment) throws ServiceException;
}
