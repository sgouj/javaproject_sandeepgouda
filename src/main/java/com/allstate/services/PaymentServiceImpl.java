package com.allstate.services;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import com.allstate.exception.DAOException;
import com.allstate.exception.InvalidPaymentRequest;
import com.allstate.exception.PaymentDetailsNotFoundException;
import com.allstate.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.rowset.serial.SerialException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService{

    @Autowired
    private PaymentDao paymentDao;


    @Override
    public long rowCount() throws ServiceException {
        try{
            return paymentDao.rowCount();
        } catch(DAOException exception) {
            throw new ServiceException(exception.getMessage(),exception);
        }

    }

    @Override
    public Payment findById(int id)  {

        Payment payment = null;

        if (id > 0) {
            payment = paymentDao.findById(id);

            if (payment == null) {
                throw new PaymentDetailsNotFoundException("No Payment details found for id: "+id);
            }
        } else {
            throw new InvalidPaymentRequest("Id should not be 0");
        }

//        try{
//            return id > 0 ? paymentDao.findById(id) : null;
//        } catch(DAOException exception) {
//            throw new ServiceException(exception.getMessage(),exception);
//        }
        return payment;

    }

    @Override
    public List<Payment> findByType(String type) throws ServiceException{
        try{
            return type!=null ? paymentDao.findByType(type):null;
        } catch(DAOException exception) {
            throw new ServiceException(exception.getMessage(),exception);
        }

    }

    @Override
    public int save(Payment payment) throws ServiceException{
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String formattedDate = formatter.format(payment.getPaymentDate());
            payment.setPaymentDate(formatter.parse(formattedDate));
            return paymentDao.save(payment);
        } catch(DAOException | ParseException exception) {
            throw new ServiceException(exception.getMessage(),exception);
        }
    }
}
