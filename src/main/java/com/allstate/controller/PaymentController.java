package com.allstate.controller;

import com.allstate.entities.Payment;
import com.allstate.exception.ServiceException;
import com.allstate.services.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/v1/payment")
public class PaymentController {

    Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    PaymentService paymentService;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        logger.info("starting method status ");
        return "Payment Rest Api is running";

    }

    @RequestMapping(value = "/rowCount", method = RequestMethod.GET)
    public long rowCount() {
        logger.info("starting rowcount ");
        long count = 0;
        try {
            paymentService.rowCount();
        } catch(ServiceException ex){
            ex.getMessage();
        }
        logger.info("Completed rowcount ");
        return count;
    }

    @RequestMapping(value="/find", method = RequestMethod.GET)
    public Payment findById(@RequestParam int id) {
        logger.info("starting findById ");
        Payment payment = paymentService.findById(id);
        logger.info("findById completed");
        return payment;
    }

    @RequestMapping(value="/type", method = RequestMethod.GET)
    public List<Payment> findByType(@RequestParam String type) {
        logger.info("starting findByType ");
        List<Payment> paymentsList = null;
        try{
            paymentsList = paymentService.findByType(type);
        } catch(Exception e) {
            e.getMessage();
        }
        logger.info("Completed findByType ");
        return paymentsList;
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public int save(@RequestBody Payment payment) {
        logger.info("starting method save ");
        int saveDone = 0;
        try{
            saveDone = paymentService.save(payment);
        } catch(ServiceException ex){
            ex.getMessage();
        }
        return saveDone;
    }

    private void logException(Exception e) {

    }
}
